<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebModel extends CI_Model {

	public function __construct()
	{
		parent:: __construct();   

    if ($this->session->userdata('site_lang')) { 
      $siteLang = $this->session->userdata('site_lang'); 
    } else { 
      $siteLang = 'english'; 
    }
	}

  public function getBannerImages()
  {  

    $query = $this ->db ->select('*')
                        ->from('banner_images') 
                        ->where('status', 1)  
                        ->order_by('cratedAT','DESC')
                        ->get();

    return $query->result();
  } 

  public function districtList()
  {   
    $query = $this ->db ->select('*')
                        ->from('district_list') 
                        ->where('isactive', 1)   
                        ->get(); 
    return $query->result();
  } 

}
  