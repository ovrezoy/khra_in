    <footer>
        <div class="footer-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <a href="#"><img id="footer_logo" src="<?= base_url(); ?>public/img/logos/logo-white.png" alt="logo"></a>
                        <p class="mt-20 text-justify">Established in 1964, the KHRA is the apex organization of the Kerala Hospitality industry. With its membership extending from the major hotel groups, boutique hotels, heritage hotels, large, medium sized and smaller hotels, restaurants, tourist homes, lodges, bakeries, tea shops & coffee shops.</p>
                        <ul class="social-links-footer">
                            <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <h2>Extra Links</h2>
                        <div class="row mt-25">
                            <div class="col-md-6 col-sm-6">
                                <ul class="footer-nav">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Vision & Mission</a></li>
                                    <li><a href="#">Case Studies</a></li>
                                    <li><a href="#">Our team</a></li>
                                    <li><a href="#">Our approach</a></li>
                                    <li><a href="#">Accounting</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <ul class="footer-nav">
                                    <li><a href="#">Business</a></li>
                                    <li><a href="#">Consulting</a></li>
                                    <li><a href="#">Development</a></li>
                                    <li><a href="#">Case Studies</a></li>
                                    <li><a href="#">Latest News</a></li>
                                    <li><a href="#">Contact us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-md-3 col-sm-6 col-12">
                        <h2>Recent news</h2>
                        <ul class="footer-news mt-25">
                            <li> <a href="#">Apartamento at ten: a decade of celebrating the everyday.</a> <strong><i class="fa fa-calendar"></i> 11 September 2018</strong> </li>
                            <li> <a href="#">Within the construction industry as their overdraft</a> <strong><i class="fa fa-calendar"></i> 11 September 2018</strong> </li>
                        </ul>
                    </div-->
                    <div class="col-md-4 col-sm-12 col-12">
                        <h2>Subscribe</h2>
                        <form class="footer-subscribe-form mt-25">
                            <div class="d-table full-width">
                                <div class="d-table-cell">
                                    <input type="text" placeholder="Your Email adress"> </div>
                                <div class="d-table-cell">
                                    <button type="submit"><i class="fas fa-envelope"></i></button>
                                </div>
                            </div>
                        </form>
                        <p class="mt-10">Get latest updates and offers.</p>
                    </div>
                </div>
                <div class="footer-1-bar">
                    <p>SpecThemes © 2019. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </footer><a href="#" class="scroll-to-top"><i class="fas fa-chevron-up"></i></a>
    <script src="<?= base_url(); ?>public/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>public/js/plugins.js"></script>
    <script src="<?= base_url(); ?>public/js/Chart.bundle.js"></script>
    <script src="<?= base_url(); ?>public/js/utils.js"></script>
    <script src="<?= base_url(); ?>public/js/navigation.js"></script>
    <script src="<?= base_url(); ?>public/js/navigation.fixed.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/jquery.themepunch.tools.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/jquery.themepunch.revolution.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.actions.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.carousel.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.kenburn.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.layeranimation.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.migration.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.parallax.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.navigation.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.slideanims.min.js"></script>
    <script src="<?= base_url(); ?>public/js/rev-slider/revolution.extension.video.min.js"></script> 
    <!-- Gallery -->
    <script src="<?= base_url(); ?>public/assets/lightGallery/dist/lightgallery.umd.js"></script>
    <script src="<?= base_url(); ?>public/assets/lightGallery/dist/plugins/zoom/lg-zoom.umd.js"></script>
    <script src="<?= base_url(); ?>public/assets/justifiedGallery/jquery.justifiedGallery.min.js"></script>
    <script src="<?= base_url(); ?>public/assets/lightGallery/dist/plugins/thumbnail/lg-thumbnail.umd.js"></script>   
    <script type="text/javascript">
        jQuery("#animated-thumbnails-gallery")
          .justifiedGallery({
            captions: false,
            lastRow: "hide",
            rowHeight: 180,
            margins: 5
          })
          .on("jg.complete", function () {
            window.lightGallery(
              document.getElementById("animated-thumbnails-gallery"),
              {
                autoplayFirstVideo: false,
                pager: false,
                galleryId: "nature",
                plugins: [lgZoom, lgThumbnail],
                mobileSettings: {
                  controls: false,
                  showCloseIcon: false,
                  download: false,
                  rotate: false
                }
              }
            );
          }); 
    </script>
    <script src="<?= base_url(); ?>public/js/map.js"></script>
    <script src="<?= base_url(); ?>public/js/main.js"></script>
</body> 

</html>