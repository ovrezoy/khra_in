
<div class="action-box action-box-md grey-bg center-holder-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-12">
                <h4 class="bold">Join Hands With K.H.R.A</h4>
                <p>K.H.R.A is a potent combination of the commitment of hotel owners on one hand and hard-core professionalism of hotel managers on the other. From Industry veterans to budding hoteliers .</p>
            </div>
            <div class="col-md-4 col-sm-4 col-12 text-right center-holder-xs mt-10"> 
                <a href="<?= base_url('web/becomeaMember'); ?>" class="button-md button-primary-bordered">Become A Member</a> 
            </div>
        </div>
    </div>
</div>