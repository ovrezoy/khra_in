<div id="preloader">
    <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div id="top-bar" class="hidden-md-down">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-12">
                <ul class="top-bar-info">   
                    <li><i class="fas fa-phone"></i> Phone: +91484-236-6602</li> 
                    <li><i class="fa fa-envelope"></i>Email: khra87@gmail.com</li>  
                    <!--li><i class="fas fa-map-marker-alt"></i>K.H.R.A. Bhavan, MG Road 682035</li-->
                </ul>
            </div>
            <div class="col-md-3 col-12">
                <ul class="social-icons hidden-sm">
                    <li><a href="https://www.facebook.com/KHRAMEDIA"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<header>
    <nav id="navigation4" class="container navigation">
        <div class="nav-header">
            <a class="nav-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url(); ?>public/img/logos/logo.png" class="main-logo" alt="logo" id="main_logo" style="max-width: 70px;">
            </a>
            <div class="nav-toggle"></div>
        </div>
        <div class="nav-menus-wrapper">
            <ul class="nav-menu align-to-right">
                <li><a href="<?= base_url(); ?>"><?= $this->lang->line('home'); ?></a></li>
                <li><a href="#"><?= $this->lang->line('about'); ?></a>
                    <ul class="nav-dropdown">
                        <li><a href="<?= base_url('web/historyofKHRA'); ?>"><?= $this->lang->line('history_of_khra'); ?></a></li>
                        <li><a href="<?= base_url('web/visionANDmission'); ?>"><?= $this->lang->line('vision_mission'); ?></a></li>
                        <li><a href="#"><?= $this->lang->line('board_members'); ?></a>
                            <ul class="nav-dropdown">
                                <li><a href="<?= base_url('web/stateCommittee'); ?>"><?= $this->lang->line('state_commitee'); ?></a></li>
                                <li><a href="<?= base_url('web/districtCommittee'); ?>"><?= $this->lang->line('district_commitee'); ?></a></li> 
                            </ul>
                        </li>
                        <!--li><a href="#">Service Of KHRA</a></li-->
                    </ul>
                </li>
                <li><a href="#">Membership</a>
                    <ul class="nav-dropdown">
                        <li><a href="<?= base_url('web/benefitsofMembers'); ?>">Benefits of a Member</a></li>
                        <li><a href="<?= base_url('web/becomeaMember'); ?>">Become a Member</a></li>
                        <li><a href="<?= base_url('web/downloadForms'); ?>">Download Forms</a></li>
                    </ul>
                </li>
                <li><a href="#">News & Events</a>
                    <ul class="nav-dropdown">
                        <li><a href="<?= base_url('web/pressRelease'); ?>">Press Release</a></li>
                        <li><a href="<?= base_url('web/newsEvents'); ?>">News & Events</a></li> 
                    </ul>
                </li>
                <li><a href="<?= base_url('web/gallery'); ?>">Gallery</a></li>
                <li><a href="#">Rezoy</a>
                    <ul class="nav-dropdown">
                        <li><a href="//www.rezoy.app/en/content/aboutus" target="_blank">About Rezoy</a></li>
                        <li><a href="//rezoy.app/" target="_blank">Order Now</a></li> 
                    </ul>
                </li>
                <li><a href="<?= base_url('web/strive'); ?>">STRIVE</a></li>
                <li><a href="<?= base_url('web/contact'); ?>">Contact Us</a></li> 
                <!--li><a href="#">Links</a>
                    <ul class="nav-dropdown">
                        <li><a href="https://tnha.in/" target="_blank">THRA</a></li>
                        <li><a href="https://www.shra.org/" target="_blank">SHRA</a></li>
                        <li><a href="https://www.hrani.net.in/" target="_blank">HRANI</a></li>
                        <li><a href="https://nrai.org/" target="_blank">NRAI</a></li>
                    </ul>
                </li-->
                <li><a href="#"><i class="fas fa-language"></i></a>
                    <ul class="nav-dropdown">
                        <li><a href="<?= base_url('LanguageSwitcher/switchLang/english'); ?>">English</a></li>
                        <li><a href="<?= base_url('LanguageSwitcher/switchLang/malayalam'); ?>">മലയാളം</a></li> 
                    </ul>
                </li> 
            </ul>
        </div>
    </nav>
</header>