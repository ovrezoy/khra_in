<!DOCTYPE html>
<html lang="en"> 

<head>
    <title>Kerala Hotel & Restaurant Association (KHRA) </title>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?= base_url(); ?>public/img/logos/logo-shortcut.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/fontawesome-all.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/icomoon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/plugins.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/rev-settings.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/custom.css"> 
    <!-- Gallery--> 
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/lightGallery/dist/css/lightgallery-bundle.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/justifiedGallery/justifiedGallery.min.css"> 
     
    <style type="text/css">
        .gallery {
          padding: 40px;
          background-image: linear-gradient(#e8f0ff 0%, white 52.08%);
          color: #0e3481;
          min-height: 100vh;
        }

        .gallery .header .lead {
          max-width: 620px;
        }

        /** Below CSS is completely optional **/

        .gallery .gallery-item {
          width: 200px;
          padding: 5px;
        }
    </style>
</head> 