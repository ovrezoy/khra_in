
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>District Committee Members</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">About</a></li>
                <li><a href="#">District Committee Members</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="row primary-form-2"> 
                <div class="col-md-9 col-12">
                    <span>Selct District</span><br/>
                    <select name="subject">
                        <option disabled>District Committee Members</option>
                        <?php foreach($district as $district) { ?>
                            <option value="<?= $district->district ?>"><?= $district->district ?></option>
                        <?php } ?>
                    </select>
                </div> 
                <div class="col-md-3 col-12">
                    <br/>
                    <button type="submit" class="button-sm button-primary">Search For Members</button>
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> 
                        <img src="<?= base_url(); ?>public/img/team/district/placeholder.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Name</h4>
                            <h6>Designation</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> 
                        <img src="<?= base_url(); ?>public/img/team/district/placeholder.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Name</h4>
                            <h6>Designation</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> 
                        <img src="<?= base_url(); ?>public/img/team/district/placeholder.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Name</h4>
                            <h6>Designation</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> 
                        <img src="<?= base_url(); ?>public/img/team/district/placeholder.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Name</h4>
                            <h6>Designation</h6> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
