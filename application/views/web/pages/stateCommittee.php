
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>State Committee Members</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">About</a></li>
                <li><a href="#">State Committee Members</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container"> 
            <div class="row"> 
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/MoideenKuttyHaji.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>MOIDEEN KUTTY HAJI</h4>
                            <h6>President</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-6 col-12 my-auto"> 
                    <blockquote>The man who comes back through the door in the wall will never be quite the same as the man who went out.
                        <span>MOIDEEN KUTTY HAJI</span>
                    </blockquote> 
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-9 col-sm-6 col-12 my-auto"> 
                    <blockquote>The man who comes back through the door in the wall will never be quite the same as the man who went out.
                        <span>G. JAYAPAL</span>
                    </blockquote> 
                </div>
                <div class="col-md-3 col-sm-6 col-12 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/GJayapal.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>G. JAYAPAL</h4>
                            <h6>Gen. Secretary</h6> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/BalakrishnaPoduval.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>K. P. BALAKRISHNA PODUVAL</h4>
                            <h6>Treasurer</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-6 col-12 my-auto"> 
                    <blockquote>The man who comes back through the door in the wall will never be quite the same as the man who went out.
                        <span>K. P. BALAKRISHNA PODUVAL</span>
                    </blockquote> 
                </div>
            </div>

            <div class="row"> 
                <div class="col-md-9 col-sm-6 col-12 my-auto"> 
                    <blockquote>The man who comes back through the door in the wall will never be quite the same as the man who went out.
                        <span>G. SUDHIESH KUMAR</span>
                    </blockquote> 
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/SudhieshKumar.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>G. SUDHIESH KUMAR</h4>
                            <h6>Patron</h6> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">  
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Prasad.jpg" alt="PRASAD ANANDABHAVAN">
                        <div class="team-box-2-info">
                            <h4>PRASAD ANANDABHAVAN</h4>
                            <h6>Working President</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/GKPrakash.jpg" alt="G. K. PRAKASH">
                        <div class="team-box-2-info">
                            <h4>G. K. PRAKASH</h4>
                            <h6>Working President</h6> 
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row"> 
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/CJCharley.jpg" alt="C. J. CHARLEY">
                        <div class="team-box-2-info">
                            <h4>C. J. CHARLEY</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/JayadharanNair.jpg" alt="B. JAYADHARAN NAIR">
                        <div class="team-box-2-info">
                            <h4>B. JAYADHARAN NAIR</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Dileep.jpg" alt="DILEEP C. MOOLAYIL">
                        <div class="team-box-2-info">
                            <h4>DILEEP C. MOOLAYIL</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/MNBabu.jpg" alt="M.N. BABU">
                        <div class="team-box-2-info">
                            <h4>M.N. BABU</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div> 
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/AbdulRazak.jpg" alt="N. ABDHUL RAZAK">
                        <div class="team-box-2-info">
                            <h4>N. ABDHUL RAZAK</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div> 
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/MuhammedSuhail.jpg" alt="MUHAMMED SUHAIL">
                        <div class="team-box-2-info">
                            <h4>MUHAMMED SUHAIL</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/MuhammedSherif.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>MUHAMMED SHERIEF</h4>
                            <h6>Vice President</h6> 
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- SECRETARY LIST -->

            <div class="row"> 
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/KMRaja.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>K. M. RAJA</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Sugunan.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>N. SUGUNAN</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Bijulal.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>C. BIJULAL</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Vijayakumar.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>VIJAYAKUMAR</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Bahulayan.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>T. S. BAHULEYAN</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/AbduRahim.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>P.P. ABDU REHIMAN</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Unni.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>P. R. UNNI</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Hariharan.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>V. T. HARIHARAN</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Faizal.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>K. K. FAIZAL</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12 mx-auto">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/state/Abdulla.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>K. H. ABDULLA</h4>
                            <h6>Secretary</h6> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
