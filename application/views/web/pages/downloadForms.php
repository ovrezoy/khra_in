
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Download Forms</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">Membership</a></li>
                <li><a href="#">Download Forms</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container"> 
            <div class="row mt-30">
                <div class="col-12"> 
                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Form Name</th>
                                <th scope="col">Link</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="Life Membership Form.pdf" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-download"></i>
                                    </a>
                                </td> 
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="Life Membership Form.pdf" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-cloud-download-alt"></i>
                                    </a>
                                </td> 
                            </tr> 
                            <tr>
                                <th scope="row">3</th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="Life Membership Form.pdf" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-external-link-alt"></i>
                                    </a>
                                </td> 
                            </tr> 
                            <tr>
                                <th scope="row">4</th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="Life Membership Form.pdf" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-link"></i>
                                    </a>
                                </td> 
                            </tr> 
                        </tbody>
                    </table> 
                </div> 
            </div>
        </div>
    </div>

    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
