
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Contact Us</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li>  
                <li><a href="#"> Contact Us</a></li>
            </ul>
        </div>
    </div>
     <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="contact-box-info-4"> <i class="icon-telephone"></i>
                        <h4>Telephone</h4> 
                        <h5>(+91) 484 2366602</h5> 
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="contact-box-info-4"> <i class="icon-fax"></i>
                        <h4>Fax</h4> 
                        <h5>(+91) 484 2362586</h5> 
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="contact-box-info-4 contact-box-info-4-center"> <i class="icon-mail-envelope-opened-outlined-interface-symbol"></i>
                        <h4>E-mail</h4> 
                        <h5>admin@khra.in</h5>  
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="contact-box-info-4"> <i class="icon-location"></i>
                        <h4>Our Location</h4> 
                        <h5>2nd Floor, K.H.R.A. Bhavan, M.G.Road Kochi - 682 035</h5> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pl-0 pr-0">
        <div class="row no-gutters">
            <div class="col-md-6 col-sm-12 col-12">
                <div class="padding-10-perc">
                    <div class="section-heading text-left"> <small class="grey-color font-size-20 font-weight-normal">Get In Touch</small>
                        <h3 class="semi-bold font-size-32">Let's walk together</h3>
                        <div class="section-heading-line line-thin"></div>
                    </div>
                    <form method="post" action="#" class="primary-form-3 mt-45">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-12">
                                <input type="text" name="name" id="name" placeholder="Name*" required> 
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                                <input type="tel" name="phone" id="phone" placeholder="Phone Number* (708 870 0700)" pattern="[0-9]{10}" required> 
                            </div>
                            <div class="col-12"> 
                                <input type="text" name="subject" id="subject" placeholder="Subject*" required> 
                            </div>
                            <div class="col-12">
                                <textarea placeholder="Message*" name="message" required></textarea>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="button-md button-primary text-uppercase ml-0">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-12">
                <div class="full-background min-350">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3915.893910760698!2d76.0889130147083!3d11.046580257169765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba635848470c61f%3A0x2e15c6723719049d!2sKerala%20Hotel%20and%20Restaurant%20Association%20(KHRA)!5e0!3m2!1sen!2sus!4v1631477704336!5m2!1sen!2sus" class="full-width full-height" loading="lazy"></iframe> 
                </div>
            </div>
        </div>
    </div>