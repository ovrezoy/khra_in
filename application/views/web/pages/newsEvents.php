
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Press Release</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">News &amp; Events</a></li>
                <li><a href="#">Press Release</a></li>
            </ul>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> 
                                <img src="<?= base_url(); ?>public/img/content/blog/b-5.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 24, 2018</h6> </div>
                                <h4>
                                    <a href="<?= base_url('web/post'); ?>">Advices for young designers, what Planning ...</a>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <a href="<?= base_url('web/post'); ?>" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> 
                                <img src="<?= base_url(); ?>public/img/content/blog/b-6.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 13, 2018</h6> </div>
                                <h4><a href="<?= base_url('web/post'); ?>">Leverage Customer Analytics, red Is Good...</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <a href="<?= base_url('web/post'); ?>" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> <img src="<?= base_url(); ?>public/img/content/blog/b-7.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 30, 2018</h6> </div>
                                <h4>
                                    <a href="<?= base_url('web/post'); ?>">The Best Way to Align, How Brands Can Get ahead ?</a>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <a href="<?= base_url('web/post'); ?>" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> 
                                <img src="<?= base_url(); ?>public/img/content/blog/b-8.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 30, 2018</h6> </div>
                                <h4>
                                    <a href="<?= base_url('web/post'); ?>">We make up the news, so you don't have to!</a>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><a href="#" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> <img src="<?= base_url(); ?>public/img/content/blog/b-1.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 24, 2018</h6> 
                                </div>
                                <h4><a href="<?= base_url('web/post'); ?>">Advices for young designers, what Planning ...</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <a href="<?= base_url('web/post'); ?>" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="blog-grid"> <img src="<?= base_url(); ?>public/img/content/blog/b-2.jpg" alt="blog">
                                <div class="blog-team-box">
                                    <h6>Oct 13, 2018</h6> </div>
                                <h4><a href="<?= base_url('web/post'); ?>">Leverage Customer Analytics, red Is Good...</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <a href="<?= base_url('web/post'); ?>" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a> </div>
                        </div>
                    </div>
                    <div class="pagination mt-20"> <a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> </div>
                </div>
            </div>
        </div>
    </div>