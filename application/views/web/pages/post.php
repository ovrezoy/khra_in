
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Title Of The Post</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="<?= base_url('web/newsEvents'); ?>">News &amp; Events</a></li>
                <li><a href="#"> Title Of The Post</a></li>
            </ul>
        </div>
    </div>
    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="blog-list"> <img src="<?= base_url(); ?>public/img/banners/third.jpg" alt="img">
                        <h4><a href="#">The Best Way to Align, How Brands Can Get ahead ?</a></h4>
                        <ul class="blog-list-info">
                            <li><i class="ti-user"></i><span>Admin</span></li>
                            <li><i class="ti-calendar"></i><span>13 May 2021</span></li>
                            <li><i class="ti-pin-alt"></i><span>Online Business</span></li>
                        </ul>
                        <p class="mt-25"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <blockquote> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </blockquote>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>