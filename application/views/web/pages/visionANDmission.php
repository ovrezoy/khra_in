
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Our Vision &amp; Mission</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">About</a></li>
                <li><a href="#">Vision &amp; Mission</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-12"> <img src="<?= base_url(); ?>public/img/about/visionmission-1.jpg" class="rounded-border" alt=""> </div>
                <div class="col-md-5 col-sm-5 col-12">
                    <div class="pl-30-md">
                        <div class="section-heading mt-15">
                            <div class="icon-holder-md"> <i class="fa fa-eye"></i> </div>
                            <h4 class="semi-bold mt-30">Our Vision</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row reverse-xs mt-70">
                <div class="col-md-5 col-sm-5 col-12">
                    <div class="pr-30-md">
                        <div class="section-heading mt-15">
                            <div class="icon-holder-md"> <i class="fa fa-bullseye" aria-hidden="true"></i> </div>
                            <h4 class="semi-bold mt-30">Our Mission</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7 col-12"> <img src="<?= base_url(); ?>public/img/about/visionmission-1.jpg" class="rounded-border" alt=""> </div>
            </div> 
        </div>
    </div> 
    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
