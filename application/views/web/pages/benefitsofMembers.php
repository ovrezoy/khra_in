
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Benefits of a Member</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">Membership</a></li>
                <li><a href="#">Benefits of a Member</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="section-heading text-center">  
                <h2 class="semi-bold font-size-35">Members Benifits</h2>
                <div class="section-heading-line line-thin"></div>
            </div>
            <div class="row mt-30">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="features-box-2">
                        <div class="features-box-2-icon"> <i class="icon-search-2"></i> </div>
                        <h4>Lorem ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="features-box-2">
                        <div class="features-box-2-icon"> <i class="icon-megaphone"></i> </div>
                        <h4>Lorem ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="features-box-2">
                        <div class="features-box-2-icon"> <i class="icon-graph"></i> </div>
                        <h4>Lorem ipsum</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="features-box-2">
                        <div class="features-box-2-icon"> <i class="icon-wallet"></i> </div>
                        <h4>Lorem ipsum </h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
