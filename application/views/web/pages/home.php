
<body class="grey-bg"> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 
    
    <div class="rev_slider_wrapper">
        <div id="rev_slider" class="rev_slider fullscreenbanner">
            <ul>
                <li data-delay="5000" data-transition="slotzoom-horizontal" data-slotamount="3" data-masterspeed="1000" data-fsmasterspeed="1000"><img src="<?= base_url(); ?>public/img/banners/banner_1.webp" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"> 
                </li>
                <li data-delay="5000" data-transition="slotzoom-horizontal" data-slotamount="3" data-masterspeed="1000" data-fsmasterspeed="1000"><img src="<?= base_url(); ?>public/img/banners/banner_2.webp" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"> 
                </li>
                <li data-delay="5000" data-transition="slotzoom-horizontal" data-slotamount="3" data-masterspeed="1000" data-fsmasterspeed="1000"><img src="<?= base_url(); ?>public/img/banners/banner_3.webp" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"> 
                </li>
                <li data-delay="5000" data-transition="slotzoom-horizontal" data-slotamount="3" data-masterspeed="1000" data-fsmasterspeed="1000"><img src="<?= base_url(); ?>public/img/banners/banner_4.webp" alt="" data-bgposition="center right" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"> 
                </li>
            </ul>
        </div>
    </div>
    <div class="section-block about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-12"> <img src="<?= base_url(); ?>public/img/about/about_khra.JPG" class="extra-plus-rounded-border shadow-primary" alt=""> </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="pl-30-md">
                        <div class="section-heading text-left mt-5">
                            <h3 class="semi-bold font-size-32"> <?= $this->lang->line('welcome_message'); ?> <br/>All About <span class="primary-color">KHRA</span></h3>
                            <div class="section-heading-line line-thin"></div>
                            <div class="text-content">
                                <p>Established in 1964, the Kerala Hotel and Restaurant Association (KHRA) is the apex organization of the Kerala Hospitality industry. With its membership extending from the major hotel groups, boutique hotels, heritage hotels, large, medium sized and smaller hotels, restaurants, tourist homes, lodges, bakeries, tea shops & coffee shops. It represents the entire spectrum of the industry.</p>
                            </div>
                        </div>
                        <div class="primary-list mt-25">
                            <ul>
                                <li><i class="fas fa-check-circle"></i>Committee is charged with experience and fresh ideas.</li>
                                <li><i class="fas fa-check-circle"></i>KHRA Members are industry veterans to budding hoteliers.</li>
                                <li><i class="fas fa-check-circle"></i>KHRA is a potent combination of the commitment of hotel owners to professionalism of hotel managers.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-40">    
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <div class="service-icon">
                            <span><i class="icon-locked-combination-padlock-stroke"></i></span>
                        </div>
                        <h3 class="title">Web Design</h3>
                        <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi, dolore quisquam illum.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox red">
                        <div class="service-icon">
                            <span><i class="fa fa-rocket"></i></span>
                        </div>
                        <h3 class="title">Web Development</h3>
                        <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi, dolore quisquam illum.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <div class="service-icon">
                            <span><i class="icon-marketing"></i></span>
                        </div>
                        <h3 class="title">Web Design</h3>
                        <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi, dolore quisquam illum.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox red">
                        <div class="service-icon">
                            <span><i class="icon-chess"></i></span>
                        </div>
                        <h3 class="title">Web Development</h3>
                        <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit. Qui quaerat fugit quas veniam perferendis repudiandae sequi, dolore quisquam illum.</p>
                    </div>
                </div>  
            </div>
        </div>  
    </div>
    <!--div class="card d-none d-lg-block d-xl-block" style="z-index: 5; top: -100px; margin: 0 100px 0 100px;">
        <div class="card-body">
            <div class="section-heading text-center">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12 countup-border-bottom-color p-b-5">
                        <div class="countup-box">
                            <h4 class="countup">35,000</h4>
                            <h5>Number of Members</h5> <i class="icon-collaboration"></i> 
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h4 class="countup">600</h4>
                            <h5>Number of Units</h5><i class="icon-locked-combination-padlock-stroke"></i> 
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h3 class="countup">1964</h3>
                            <h5>Established Year</h5><i class="icon-discussion2"></i> 
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h4 class="countup">28</h4>
                            <h5>Secretariat Members</h5><i class="icon-award"></i> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <!-- <div class="section-block background-count"> --> 
    <div class="section-block d-none d-xs-block d-sm-block">
        <div class="container">
            <div class="section-heading text-center">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12 p-b-5">
                        <div class="countup-box">
                            <h4 class="countup">35,000</h4>
                            <h5>Number of Members</h5> <i class="icon-collaboration"></i> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h4 class="countup">600</h4>
                            <h5>Number of Units</h5><i class="icon-locked-combination-padlock-stroke"></i> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h4>1964</h3>
                            <h5>Established Year</h5><i class="icon-discussion2"></i> </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="countup-box">
                            <h4 class="countup">28</h4>
                            <h5>Secretariat Members</h5><i class="icon-award"></i> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="section-block grey-bg background-shape-3">
        <div class="container">
            <div class="section-heading text-center">
                <h3 class="semi-bold font-size-33">Hear From Our Members.</h3>
                <div class="section-heading-line line-thin"></div> 
            </div>
            <div class="owl-carousel owl-theme testmonial-carousel-4 mt-20">
                <div class="testmonial-item-5">
                    <div class="testmonial-item-5-img"> <img src="<?= base_url(); ?>public/img/content/testmonials/t-4.jpg" alt="img"> </div>
                    <div class="testmonial-item-5-name">
                        <h4>Andrew Bright</h4> <span>Designer</span> </div>
                    <div class="testmonial-item-5-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    </div>
                    <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fas fa-star"></i> </div>
                </div>
                <div class="testmonial-item-5">
                    <div class="testmonial-item-5-img"> <img src="<?= base_url(); ?>public/img/content/testmonials/t-2.jpg" alt="img"> </div>
                    <div class="testmonial-item-5-name">
                        <h4>Andrew Bright</h4> <span>Designer</span> </div>
                    <div class="testmonial-item-5-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    </div>
                    <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fas fa-star"></i> </div>
                </div>
                <div class="testmonial-item-5">
                    <div class="testmonial-item-5-img"> <img src="<?= base_url(); ?>public/img/content/testmonials/t-3.jpg" alt="img"> </div>
                    <div class="testmonial-item-5-name">
                        <h4>Andrew Bright</h4> <span>Designer</span> </div>
                    <div class="testmonial-item-5-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    </div>
                    <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fas fa-star"></i> </div>
                </div>
                <div class="testmonial-item-5">
                    <div class="testmonial-item-5-img"> <img src="<?= base_url(); ?>public/img/content/testmonials/t-1.jpg" alt="img"> </div>
                    <div class="testmonial-item-5-name">
                        <h4>Andrew Bright</h4> <span>Designer</span> </div>
                    <div class="testmonial-item-5-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    </div>
                    <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fas fa-star"></i> </div>
                </div>
            </div>
        </div>
    </div>
     
    <div class="section-block">
        <div class="container">
            <div class="section-heading text-center">
                <h3 class="semi-bold">Recent News & Events</h3>
                <div class="section-heading-line line-thin dark-bg"></div>
                <p>Latest News and event of KHRA.</p>
            </div>
            <div class="row mt-50">
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="blog-grid"><img src="<?= base_url(); ?>public/img/banners/banner_1.webp" alt="blog">
                        <div class="blog-team-box">
                            <h6>Nov 24, 2020</h6></div>
                        <h4><a href="#">Soft-Launch Of Rezoy App</a></h4>
                        <p>Rezoy App is a KHRA initiative for the normal people and restaurents to destroy the monopoly of multi-national companies.</p><a href="#" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a></div>
                </div>
                <!--div class="col-md-4 col-sm-4 col-12">
                    <div class="blog-grid"><img src="<?= base_url(); ?>public/img/content/blog/b-2.jpg" alt="blog">
                        <div class="blog-team-box">
                            <h6>Oct 13, 2018</h6></div>
                        <h4><a href="#">Leverage Customer Analytics, red Is Good Smart Leader ...</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><a href="#" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a></div>
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <div class="blog-grid"><img src="<?= base_url(); ?>public/img/content/blog/b-3.jpg" alt="blog">
                        <div class="blog-team-box">
                            <h6>Oct 30, 2018</h6></div>
                        <h4><a href="#">The Best Way to Align, How Brands Can Get ahead ?</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><a href="#" class="button-simple-primary mt-20">Read More <i class="fas fa-arrow-right"></i></a></div>
                </div-->
            </div>
        </div>
    </div>
                <!--<div class="col-md-3 col-sm-6 col-12">
                    <div class="section-heading mt-30-xs">
                        <h6 class="semi-bold">Modal Popup Flip</h6> </div>
                    <div class="mt-20">
                        <button class="button-sm button-primary-bordered" data-izimodal-open="#modal4"> Open Modal </button>
                        <div class="izimodal" id="modal4" data-iziModal-width="400px" data-iziModal-fullscreen="true" data-iziModal-transitionIn="flipInX" data-iziModal-transitionOut="flipOutX">
                            <div class="close-modal">
                                <button class="close-modal" data-izimodal-close><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-inside inner-30 center-holder">
                                <div class="modal-heading">
                                    <h4>Modal Dialog</h4> </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                </div>-->
    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 