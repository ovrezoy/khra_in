
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Press Release</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">News &amp; Events</a></li>
                <li><a href="#">Press Release</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container"> 
            <div class="section-heading text-center"> 
                <h2 class="semi-bold font-size-35">Press Release</h2>
                <div class="section-heading-line line-thin"></div>
            </div>
            <div class="row mt-30">
                <div class="col-12"> 
                    <table class="table text-center"> 
                        <tbody>
                            <tr>
                                <th scope="row"><i class="far fa-newspaper fa-3x"></i></th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="#" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-link"></i> Read More
                                    </a>
                                </td> 
                            </tr>
                            <tr>
                                <th scope="row"><i class="far fa-newspaper fa-3x"></i></th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="#" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-link"></i> Read More
                                    </a>
                                </td> 
                            </tr> 
                            <tr>
                                <th scope="row"><i class="far fa-newspaper fa-3x"></i></th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="#" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-link"></i> Read More
                                    </a>
                                </td> 
                            </tr> 
                            <tr>
                                <th scope="row"><i class="far fa-newspaper fa-3x"></i></th>
                                <td>Life Membership Application Form</td>
                                <td>
                                    <a href="#" class="button-xs button-primary-bordered" target="_blank">
                                        <i class="fas fa-link"></i> Read More
                                    </a>
                                </td> 
                            </tr> 
                        </tbody>
                    </table> 
                </div> 
            </div>
        </div>
    </div>
 