
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Gallery</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">Gallery</a></li> 
            </ul>
        </div>
    </div>
    <div class="section-block grey-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-1.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Dangerous Rocks Kanga</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-9.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">The Violent Storms</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-3.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">The Bomb Squad</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-7.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Yellow Moose</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-6.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Pure Panther</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-13.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Lemon Drops</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-8.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Gray Panthers</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-4.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Green Jade</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="portolio-boxed-2"> <img src="<?= base_url(); ?>public/img/content/portfolio/p-2.jpg" alt="" class="image">
                        <div class="portolio-boxed-content">
                            <div class="d-flex">
                                <div class="portfolio-title">
                                    <h3><a href="#">Gray Panthers</a></h3>
                                    <h4>Design</h4> </div>
                                <div class="portfolio-btn"> <a href="#"><i class="ti-arrow-right"></i></a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>