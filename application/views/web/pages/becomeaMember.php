
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>Become a Member</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">Membership</a></li>
                <li><a href="#">Become a Member</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="section-heading text-center"> 
                <h2 class="semi-bold font-size-35">Membership Form</h2>
                <div class="section-heading-line line-thin"></div>
            </div>
            <form method="post" action="#" class="primary-form-3 mt-50">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <input type="text" name="name" placeholder="Name*"> </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <input type="text" name="phone" placeholder="Phone Number*"> </div>
                    <div class="col-12">
                        <select name="subject">
                            <option>Subject</option>
                            <option>Development Manager</option>
                            <option>Project Lead</option>
                            <option>Solutions Analyst</option>
                        </select>
                    </div>
                    <div class="col-12">
                        <textarea placeholder="Message*" name="message"></textarea>
                    </div>
                </div>
                <a href="#" class="button-lg button-primary">Send Request</a>
            </form>
        </div>
    </div> 
