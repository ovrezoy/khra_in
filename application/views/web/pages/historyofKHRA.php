
<body> 
    
    <!-- ========== Top Navbar Start ========== -->
    <?php $this->load->view('web/_navbar'); ?>
    <!-- ========== Top Navbar End ========== --> 

    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6" style="background-image: url(<?= base_url(); ?>public/img/content/bgs/bg-breadcrumb-1.jpg) !important;">
        <div class="container text-center">
            <h1>History Of KHRA</h1>
            <ul>
                <li><a href="<?= base_url(); ?>">Home</a></li> 
                <li><a href="#">About</a></li>
                <li><a href="#">History Of KHRA</a></li>
            </ul>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="section-heading text-left mt-15"> 
                        <small class="primary-color">Founders</small>
                        <h4 class="semi-bold">Our Foundation Stones </h4> 
                    </div>
                    <div class="text-content mt-15">
                        <p>Meet our foundation , The people who put forwarded the idea and build K.H.R.A. with a strong foundation.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/founders/GovindaRao.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Govinda Rao</h4>
                            <h6>Founder</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/founders/KeralavarmaRaja.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Keralavarma Raja</h4>
                            <h6>Founder</h6> 
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="team-box-2"> <img src="<?= base_url(); ?>public/img/team/founders/Kuriakos.jpg" alt="">
                        <div class="team-box-2-info">
                            <h4>Kuriakos</h4>
                            <h6>Founder</h6> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-block">
        <div class="container">
            <div class="section-heading text-left">
                    <div class="section-heading text-left mt-15"> 
                        <small class="primary-color">History</small>
                        <h4 class="semi-bold">KHRA Formation</h4> 
                    </div> 
                <div class="section-heading-line line-thin"></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div> 
        </div>
    </div> 

    <div class="section-block">
        <div class="container"> 
            <div class="mt-30"></div>
            <div class="timeline">
                <div class="timeline-left-all">
                    <h2>26 Dec 2015</h2>
                    <div class="timeline-container timeline-left">
                        <div class="timeline-content">
                            <div class="timeline-content-inner">
                                <h3>Consumer focused marketing.</h3>
                                <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-right-all">
                    <h2>05 Jan 2016</h2>
                    <div class="timeline-container timeline-right">
                        <div class="timeline-content">
                            <div class="timeline-content-inner">
                                <h3>Raise your business above the rest.</h3>
                                <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-left-all">
                    <h2>07 Ogt 2016</h2>
                    <div class="timeline-container timeline-left">
                        <div class="timeline-content">
                            <div class="timeline-content-inner">
                                <h3>Market your business to own your market.</h3>
                                <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-right-all">
                    <h2>05 Jan 2019</h2>
                    <div class="timeline-container timeline-right">
                        <div class="timeline-content">
                            <div class="timeline-content-inner">
                                <h3>Achieving marketing superiority.</h3>
                                <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ========== Join KHRA Bar ========== -->
    <?php $this->load->view('web/_becomeaMember'); ?>
    <!-- ========== Join KHRA Bar ========== --> 
