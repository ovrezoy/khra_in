<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
 
	function __construct() {
	    parent::__construct(); 
	  
		$siteLang = $this->session->userdata('site_lang');

       	if ($siteLang) { 
           $this->lang->load('information',$siteLang); 
       	} else { 
           $this->lang->load('information','english'); 
       	}

    	$this->load->Model('WebModel'); 
	}

	public function index()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/home');
		$this->load->view('web/_footer');
	}

	public function historyofKHRA()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/historyofKHRA');
		$this->load->view('web/_footer');
	}

	public function visionANDmission()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/visionANDmission');
		$this->load->view('web/_footer');
	}

	public function stateCommittee()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/stateCommittee');
		$this->load->view('web/_footer');
	}

	public function districtCommittee()
	{
		$data["district"] = $this->WebModel->districtList();

		$this->load->view('web/_header');
		$this->load->view('web/pages/districtCommittee',$data);
		$this->load->view('web/_footer');
	}

	public function benefitsofMembers()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/benefitsofMembers');
		$this->load->view('web/_footer');
	}

	public function becomeaMember()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/becomeaMember');
		$this->load->view('web/_footer');
	}

	public function downloadForms()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/downloadForms');
		$this->load->view('web/_footer');
	}

	public function pressRelease()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/pressRelease');
		$this->load->view('web/_footer');
	}

	public function newsEvents()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/newsEvents');
		$this->load->view('web/_footer');
	}

	public function post()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/post');
		$this->load->view('web/_footer');
	}

	public function gallery()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/gallery');
		$this->load->view('web/_footer');
	}

	public function viewGallery()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/gallery_single');
		$this->load->view('web/_footer');
	}

	public function contact()
	{
		$this->load->view('web/_header');
		$this->load->view('web/pages/contact');
		$this->load->view('web/_footer');
	}
}
