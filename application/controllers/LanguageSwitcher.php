<?php 

class LanguageSwitcher extends CI_Controller
{

   public function __construct() {

       parent::__construct();

   }

   function switchLang($language = "") { 

        $language = ($language != "") ? $language : "english";

        $this->session->set_userdata('site_lang', $language);

        if(isset($_SERVER['HTTP_REFERER'])) {
          redirect($_SERVER['HTTP_REFERER']); 
        }else{
          redirect('/'); 
        }
        
        //redirect($_SERVER['HTTP_REFERER']); 
   }

}